package pl.huculak.lukasz.navmiitask.tracks;

import android.support.v4.util.Pair;

import com.f2prateek.rx.preferences2.Preference;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InOrder;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringBufferInputStream;
import java.util.Arrays;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.internal.schedulers.TrampolineScheduler;
import io.reactivex.plugins.RxJavaPlugins;
import pl.huculak.lukasz.navmiitask.model.AssetsInteractor;
import pl.huculak.lukasz.navmiitask.xml.RxXMLParser;
import pl.huculak.lukasz.navmiitask.xml.XMLTagData;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

/**
 * Created by Łukasz on 01.09.2017.
 */
public class TracksPresenterImplTest {

    @BeforeClass
    public static void prepareSchedulers() {
        RxJavaPlugins.setInitComputationSchedulerHandler(schedulerCallable -> TrampolineScheduler.instance());
        RxJavaPlugins.setInitIoSchedulerHandler(schedulerCallable -> TrampolineScheduler.instance());
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> TrampolineScheduler.instance());
    }

    @Test
    public void testCombiningInputEvents() {
        Preference<String> mockFilePathPref = mock(Preference.class);
        when(mockFilePathPref.asObservable()).thenReturn(Observable.just("any"));

        TracksView mockView = mock(TracksView.class);
        when(mockView.mapReady()).thenReturn(Observable.just(Boolean.TRUE));

        TracksPresenterImpl presenter = new TracksPresenterImpl(mockFilePathPref, mock(AssetsInteractor.class));


        Observable.combineLatest(mockFilePathPref.asObservable(),
                mockView.mapReady()
                , (filePath, state) -> filePath)
                .test()
                .assertValueCount(1)
                .assertValueAt(0, "any");

    }

    @Test
    public void testTrackFilter() {
        new RxXMLParser(new InputSource(
                new StringBufferInputStream("<track><trkpt lat=\"0.0\" lon=\"0.0\"></trkpt><trkpt lat=\"1.0\" lon=\"1.0\"></trkpt></track>")
        )).toObservable()
                .filter(xmlTagData -> "trkpt" .equalsIgnoreCase(xmlTagData.getName()))
                .test()
                .assertValueCount(2)
                .assertValues(
                        new XMLTagData.Builder("trkpt").addAttribute("lat", "0.0").addAttribute("lon", "0.0").build(),
                        new XMLTagData.Builder("trkpt").addAttribute("lat", "1.0").addAttribute("lon", "1.0").build()
                );
    }

    @Test
    public void testMapping() {
        Observable.just(
                new XMLTagData.Builder("trkpt").addAttribute("lat", "0.0").addAttribute("lon", "0.0").build(),
                new XMLTagData.Builder("trkpt").addAttribute("lat", "1.0").addAttribute("lon", "1.0").build())
                .map(xmlTagData -> new Pair<>(xmlTagData.getAttribute("lat"), xmlTagData.getAttribute("lon")))
                .map(latLonPair -> new LatLng(Double.parseDouble(latLonPair.first), Double.parseDouble(latLonPair.second)))
                // lets skip duplicated positions
                .distinctUntilChanged()
                .test()
                .assertValueCount(2)
                .assertValues(new LatLng(0d, 0d), new LatLng(1d, 1d));
    }

    @Test
    public void testDistinct() {
        Observable.just(new LatLng(0d, 0d), new LatLng(1d, 1d))
                .distinctUntilChanged()
                .toList()
                .test()
                .assertValueCount(1)
                .assertValue(list -> list.size() == 2)
                .assertValue(list -> list.containsAll(Arrays.asList(new LatLng(0d, 0d), new LatLng(1d, 1d))));

    }

    @Test
    public void testMapToPolylineOptions() {
        Single.just(Arrays.asList(new LatLng(0d, 0d), new LatLng(1d, 1d)))
                .map(trackCoords -> new PolylineOptions().addAll(trackCoords))
                .toObservable()
                .test()
                .assertValueCount(1)
                .assertValue(polylineOptions -> polylineOptions.getPoints().size() == 2);
    }

    @Test
    public void testStartingWithEmptyPolylineOptions() {
        Observable.just(new PolylineOptions().add(new LatLng(0d, 0d), new LatLng(1d, 1d)))
                .startWith(new PolylineOptions())
                .test()
                .assertValueCount(2)
                .assertValueAt(0, polylineOptions -> polylineOptions.getPoints().size() == 0)
                .assertValueAt(1, polylineOptions -> polylineOptions.getPoints().size() == 2);

    }

    @Test
    public void testBindIntents() throws IOException {
        AssetsInteractor mockInteractor = mock(AssetsInteractor.class);
        when(mockInteractor.openFileFromAssets(anyString())).thenReturn(
                new StringBufferInputStream("<track><trkpt lat=\"0.0\" lon=\"0.0\"></trkpt><trkpt lat=\"1.0\" lon=\"1.0\"></trkpt></track>")
        );

        Preference<String> mockFilePathPref = mock(Preference.class);
        when(mockFilePathPref.asObservable()).thenReturn(Observable.just("any"));

        TracksView mockView = mock(TracksView.class);
        when(mockView.mapReady()).thenReturn(Observable.just(Boolean.TRUE));

        TracksPresenterImpl presenter = new TracksPresenterImpl(mockFilePathPref, mockInteractor);

        InOrder inOrder = inOrder(mockView);

        presenter.attachView(mockView);

        verify(mockView, times(2)).render(any());

        inOrder.verify(mockView).render(argThat(new BaseMatcher<TracksViewState>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("Bad track view state - empty expected");
            }

            @Override
            public boolean matches(Object item) {
                return ((TracksViewState) item).getTracks() == null;
            }
        }));

        inOrder.verify(mockView).render(argThat(new BaseMatcher<TracksViewState>() {

            @Override
            public void describeTo(Description description) {
                description.appendText("Bad track view state");
            }

            @Override
            public boolean matches(Object item) {
                return ((TracksViewState) item).getTracks() != null && ((TracksViewState) item).getTracks().getPoints().size() == 2;
            }
        }));
    }

    @Test
    public void testSwitchMapReady() {
        Object nonEmpty = new Object();
        Object EMPTY_STATE = new Object();
        Observable.just(
                Boolean.FALSE,
                Boolean.TRUE,
                Boolean.FALSE,
                Boolean.TRUE
        )
                .doOnNext(ready -> System.out.println("map ready = " + ready))
                .switchMap(mapReady ->
                {
                    if (mapReady) {
                        return Observable.just(nonEmpty);
                    } else {
                        return Observable.just(EMPTY_STATE);
                    }
                })
                .startWith(EMPTY_STATE)
                .test()
                .assertValueCount(5)
                .assertValues(EMPTY_STATE, EMPTY_STATE, nonEmpty, EMPTY_STATE, nonEmpty);
    }
}