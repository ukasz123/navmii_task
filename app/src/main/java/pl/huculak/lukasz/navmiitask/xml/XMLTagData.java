package pl.huculak.lukasz.navmiitask.xml;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * XML tag data model
 * Created by Łukasz on 30.08.2017.
 */

public final class XMLTagData {
    private String name;
    private Map<String, String> attributes;

    private XMLTagData(String name, Map<String, String> attributes) {
        this.name = name;
        this.attributes = attributes;
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public String getAttribute(String key) {
        return attributes.get(key);
    }

    @Override
    public String toString() {
        return "XMLTagData{" +
                "name='" + name + '\'' +
                ", attributes=" + attributes +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        XMLTagData that = (XMLTagData) o;

        return name.equals(that.name) && attributes.equals(that.attributes);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + attributes.hashCode();
        return result;
    }

    public static final class Builder {
        private final String tagName;
        private final HashMap<String, String> attributes;

        public Builder(@NonNull String name) {
            this.tagName = name;
            this.attributes = new HashMap<>();
        }

        public Builder attributes(@NonNull Map<String, String> attributes) {
            this.attributes.clear();
            this.attributes.putAll(attributes);
            return this;
        }

        public Builder addAttribute(@NonNull String attrName, @NonNull String attrValue) {
            this.attributes.put(attrName, attrValue);
            return this;
        }

        public XMLTagData build() {
            return new XMLTagData(this.tagName, this.attributes);
        }
    }
}
