package pl.huculak.lukasz.navmiitask.map;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.f2prateek.rx.preferences2.Preference;
import com.f2prateek.rx.preferences2.RxSharedPreferences;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.hannesdorfmann.mosby3.FragmentMviDelegate;
import com.hannesdorfmann.mosby3.FragmentMviDelegateImpl;
import com.hannesdorfmann.mosby3.MviDelegateCallback;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import pl.huculak.lukasz.navmiitask.model.AssetsInteractor;
import pl.huculak.lukasz.navmiitask.tracks.TracksPresenter;
import pl.huculak.lukasz.navmiitask.tracks.TracksPresenterImpl;
import pl.huculak.lukasz.navmiitask.tracks.TracksView;
import pl.huculak.lukasz.navmiitask.tracks.TracksViewState;

import static android.content.Context.MODE_PRIVATE;
import static pl.huculak.lukasz.navmiitask.TracksMapActivity.PREF_SELECTED_FILE;

/**
 * Map fragment that can display tracks
 * Created by Łukasz on 04.09.2017.
 */

public class TracksSupportMapFragment extends SupportMapFragment implements TracksView, MviDelegateCallback<TracksView, TracksPresenter>, OnMapReadyCallback {

    private static final String TAG = "TracksSupportFragment";
    private FragmentMviDelegate<TracksView, TracksPresenter> mviDelegate = new FragmentMviDelegateImpl<>(this, this);
    private BehaviorSubject<Boolean> mapReadySubject = BehaviorSubject.createDefault(Boolean.FALSE);
    private GoogleMap mMap;

    @NonNull
    @Override
    public TracksPresenter createPresenter() {
        Preference<String> filePathPref = RxSharedPreferences.create(getActivity().getPreferences(MODE_PRIVATE)).getString(PREF_SELECTED_FILE);
        AssetsInteractor interactor = new AssetsInteractor(getContext());

        return new TracksPresenterImpl(filePathPref, interactor);
    }

    @NonNull
    @Override
    public TracksView getMvpView() {
        return this;
    }

    @Override
    public void setRestoringViewState(boolean restoringViewState) {

    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        mapReadySubject.onNext(false);
        mviDelegate.onCreate(bundle);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        getMapAsync(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mviDelegate.onAttach(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        mviDelegate.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mviDelegate.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        mviDelegate.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mviDelegate.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapReadySubject.onNext(false);
        mviDelegate.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mviDelegate.onDestroy();
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        mviDelegate.onActivityCreated(bundle);
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        mviDelegate.onSaveInstanceState(bundle);
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        mviDelegate.onAttachFragment(childFragment);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mviDelegate.onAttach(context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mviDelegate.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mviDelegate.onConfigurationChanged(newConfig);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mviDelegate.onDetach();
    }


    @Override
    public Observable<Boolean> mapReady() {
        return mapReadySubject;
    }

    @Override
    public void render(@NonNull TracksViewState currentState) {
        Log.d(TAG, "render() called with: currentState = [" + currentState + ", " + currentState.getTracks() + "]");
        if (mMap != null && currentState.getTracks() != null) { // render only when map is available - render is called even after screen rotation when map not-ready event is send to presenter
            mMap.clear();
            mMap.addPolyline(currentState.getTracks());
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(currentState.getZoomArea(), 10));
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mapReadySubject.onNext(Boolean.TRUE);
    }

}
