package pl.huculak.lukasz.navmiitask.xml;


import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParserFactory;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;

/**
 * Helper for parsing XML input
 * Created by Łukasz on 30.08.2017.
 */

public class RxXMLParser {

    private final InputSource source;

    public RxXMLParser(InputSource source) {
        this.source = source;
    }

    public Observable<XMLTagData> toObservable() {
        return Observable.create(new ObservableOnSubscribe<XMLTagData>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<XMLTagData> e) throws Exception {
                SAXParserFactory.newInstance().newSAXParser().parse(source, new XmlTagsHandler(e));
            }
        });
    }

    private class XmlTagsHandler extends DefaultHandler {
        private final ObservableEmitter<XMLTagData> emitter;

        public XmlTagsHandler(ObservableEmitter<XMLTagData> emitter) {
            this.emitter = emitter;
        }

        @Override
        public void endDocument() throws SAXException {
            super.endDocument();
            if (!emitter.isDisposed()) {
                emitter.onComplete();
            }
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes);
            XMLTagData.Builder b = new XMLTagData.Builder( /* TextUtils.isEmpty not mocked in test*/(localName == null || localName.length() == 0) ? qName : localName);
            int attributesLength = attributes.getLength();
            for (int i = 0; i < attributesLength; i++) {
                b.addAttribute(attributes.getLocalName(i), attributes.getValue(i));
            }
            if (!emitter.isDisposed()) {
                emitter.onNext(b.build());
            }
        }

        @Override
        public void error(SAXParseException e) throws SAXException {
            super.error(e);
            if (!emitter.isDisposed()) {
                emitter.onError(e);
            }
        }

        @Override
        public void fatalError(SAXParseException e) throws SAXException {
            super.fatalError(e);
            if (!emitter.isDisposed()) {
                emitter.onError(e);
            }
        }
    }
}
