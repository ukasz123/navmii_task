package pl.huculak.lukasz.navmiitask;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.f2prateek.rx.preferences2.Preference;
import com.f2prateek.rx.preferences2.RxSharedPreferences;
import com.hannesdorfmann.mosby3.mvi.MviActivity;
import com.jakewharton.rxbinding2.widget.RxAdapterView;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import pl.huculak.lukasz.navmiitask.model.AssetsInteractor;
import pl.huculak.lukasz.navmiitask.selectFile.FilesListPresenter;
import pl.huculak.lukasz.navmiitask.selectFile.FilesListPresenterImpl;
import pl.huculak.lukasz.navmiitask.selectFile.FilesListView;
import pl.huculak.lukasz.navmiitask.selectFile.FilesListViewState;

public class TracksMapActivity extends MviActivity<FilesListView, FilesListPresenter> implements FilesListView {

    public static final java.lang.String PREF_SELECTED_FILE = "selected_file";
    private static final String TAG = "TracksMapAct";
    private Spinner filesSpinner;
    private ArrayAdapter<String> filesSpinnerAdapter;
    private FilesListPresenterImpl filesPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracks_map);

        //setup Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // setup files spinner
        filesSpinner = (Spinner) findViewById(R.id.filesSpinner);
        filesSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item);
        filesSpinner.setAdapter(filesSpinnerAdapter);

        if (filesPresenter == null) {
            Preference<String> filePathPref = RxSharedPreferences.create(getPreferences(MODE_PRIVATE)).getString(PREF_SELECTED_FILE);
            AssetsInteractor interactor = new AssetsInteractor(this);
            filesPresenter = new FilesListPresenterImpl(filePathPref, interactor);
        }

    }

    @NonNull
    @Override
    public FilesListPresenter createPresenter() {
        return filesPresenter;
    }


    @Override
    public Observable<String> selectFileIntent() {
        return RxAdapterView.itemSelections(filesSpinner)
                .filter(index -> index != AdapterView.INVALID_POSITION)
                .debounce(100, TimeUnit.MILLISECONDS)
                .map(index -> filesSpinnerAdapter.getItem(index));
    }

    @Override
    public void render(FilesListViewState state) {
        Log.d(TAG, "render: " + state.getAvailableFiles() + " -> " + state.getSelectedFile());
        if (state.getAvailableFiles() != null) {
            filesSpinnerAdapter.clear();
            filesSpinnerAdapter.addAll(state.getAvailableFiles());
            filesSpinner.setSelection(filesSpinnerAdapter.getPosition(state.getSelectedFile()));
        }
    }

}
