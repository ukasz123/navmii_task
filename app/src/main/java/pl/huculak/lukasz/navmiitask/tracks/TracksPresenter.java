package pl.huculak.lukasz.navmiitask.tracks;

import com.hannesdorfmann.mosby3.mvi.MviPresenter;

/**
 * Created by Łukasz on 30.08.2017.
 */

public interface TracksPresenter extends MviPresenter<TracksView, TracksViewState> {

}
