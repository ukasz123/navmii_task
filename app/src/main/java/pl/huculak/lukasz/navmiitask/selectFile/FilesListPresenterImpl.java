package pl.huculak.lukasz.navmiitask.selectFile;

import android.util.Log;

import com.f2prateek.rx.preferences2.Preference;
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import pl.huculak.lukasz.navmiitask.model.AssetsInteractor;

/**
 * Pr
 * Created by Łukasz on 31.08.2017.
 */

public class FilesListPresenterImpl extends MviBasePresenter<FilesListView, FilesListViewState> implements FilesListPresenter {

    private static final String TAG = "FilesListPres";
    private final Consumer<? super String> saveSelectedFileInPref;

    private final AssetsInteractor assets;
    private final Observable<String> savedFile;
    private Disposable saveFileDisposable;

    public FilesListPresenterImpl(Preference<String> selectedFilePref, AssetsInteractor assets) {
        this.saveSelectedFileInPref = selectedFilePref.asConsumer();
        this.savedFile = selectedFilePref.asObservable();
        this.assets = assets;
    }

    @Override
    protected void bindIntents() {
        Log.d(TAG, "bindIntents() called");
        saveFileDisposable = intent(FilesListView::selectFileIntent)
                .distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .subscribe(saveSelectedFileInPref);


        Observable<FilesListViewState> states = Observable.combineLatest(
                assets.getAvailableGPXFiles().subscribeOn(Schedulers.io()).toList().toObservable(),
                savedFile,
                (filesList, selected) -> new FilesListViewState.Builder()
                        .selectedFile(selected)
                        .availableFiles(filesList).build())
                .doOnNext(state -> Log.d(TAG, "file vs = " + state));

        subscribeViewState(states.observeOn(AndroidSchedulers.mainThread()), FilesListView::render);
    }

    @Override
    protected void unbindIntents() {
        Log.d(TAG, "unbindIntents: ");
        super.unbindIntents();
        if (saveFileDisposable != null) {
            saveFileDisposable.dispose();
        }
        saveFileDisposable = null;
    }
}
