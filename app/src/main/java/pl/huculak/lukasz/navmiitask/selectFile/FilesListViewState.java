package pl.huculak.lukasz.navmiitask.selectFile;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Collections;
import java.util.List;

/**
 * Holds state of FilesListView instance
 * Created by Łukasz on 31.08.2017.
 */

public class FilesListViewState {
    private List<String> availableFiles;
    private String selectedFile = null;

    private FilesListViewState(List<String> availableFiles, String selectedFile) {
        this.availableFiles = availableFiles;
        this.selectedFile = selectedFile;
    }

    public List<String> getAvailableFiles() {
        return availableFiles;
    }

    public String getSelectedFile() {
        return selectedFile;
    }

    @Override
    public String toString() {
        return "FilesListViewState{" +
                "availableFiles=" + availableFiles +
                ", selectedFile='" + selectedFile + '\'' +
                '}';
    }

    public static class Builder {


        private List<String> availableFiles = Collections.emptyList();
        private String selectedFile;

        public Builder availableFiles(@NonNull List<String> availableFiles) {
            this.availableFiles = availableFiles;
            return this;
        }

        public Builder selectedFile(@Nullable String selectedFile) {
            this.selectedFile = selectedFile;
            return this;
        }

        public FilesListViewState build() {
            return new FilesListViewState(availableFiles, selectedFile);
        }
    }
}
