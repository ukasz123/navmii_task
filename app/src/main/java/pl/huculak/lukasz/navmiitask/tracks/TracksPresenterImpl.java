package pl.huculak.lukasz.navmiitask.tracks;

import android.support.v4.util.Pair;

import com.f2prateek.rx.preferences2.Preference;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter;

import org.xml.sax.InputSource;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import pl.huculak.lukasz.navmiitask.model.AssetsInteractor;
import pl.huculak.lukasz.navmiitask.xml.RxXMLParser;

/**
 * Created by Łukasz on 30.08.2017.
 */

public class TracksPresenterImpl extends MviBasePresenter<TracksView, TracksViewState> implements TracksPresenter {

    private static final String TRKPT = "trkpt";
    private static final String ATTR_LAT = "lat";
    private static final String ATTR_LONG = "lon";
    private static final TracksViewState EMPTY_STATE = new TracksViewState.Builder().build();
    private final AssetsInteractor assetsInteractor;
    private final Observable<String> selectedFileObservable;

    public TracksPresenterImpl(Preference<String> selectedFilePref, AssetsInteractor assetsInteractor) {
        this.assetsInteractor = assetsInteractor;
        this.selectedFileObservable = selectedFilePref.asObservable();
    }

    @Override
    protected void bindIntents() {

        Observable<PolylineOptions> track = prepareTracksObservable();

        Observable<TracksViewState> viewStateWithZoom = calculateZoom(track);

        subscribeViewState(intent(TracksView::mapReady)
                        .switchMap(mapReady ->
                        {
                            if (mapReady) {
                                return viewStateWithZoom;
                            } else {
                                return Observable.just(EMPTY_STATE);
                            }
                        })
                        .startWith(EMPTY_STATE).observeOn(AndroidSchedulers.mainThread())
                , TracksView::render);
    }

    private Observable<PolylineOptions> prepareTracksObservable() {

        return selectedFileObservable
                .filter(filePath -> filePath.length() > 0)
                .observeOn(Schedulers.io())
                .map(assetsInteractor::openFileFromAssets)
                .map(InputSource::new)
                .observeOn(Schedulers.computation())
                .flatMap(source -> new RxXMLParser(source).toObservable()
                        //TODO make this parsing more flexible
                        // it may parse only one track per file
                        .filter(xmlTagData -> TRKPT.equalsIgnoreCase(xmlTagData.getName()))
                        .map(xmlTagData -> new Pair<>(xmlTagData.getAttribute(ATTR_LAT), xmlTagData.getAttribute(ATTR_LONG)))
                        .map(latLonPair -> new LatLng(Double.parseDouble(latLonPair.first), Double.parseDouble(latLonPair.second)))
                        // lets skip duplicated positions
                        .distinctUntilChanged()
                        .toList()
                        .map(trackCoords -> new PolylineOptions().addAll(trackCoords))
                        .toObservable()
                );
    }

    private Observable<TracksViewState> calculateZoom(Observable<PolylineOptions> track) {
        return track.flatMap(po ->
                Observable.zip(
                        Observable.just(po),
                        Observable.fromIterable(po.getPoints())
                                .reduce(new LatLngBounds.Builder(),
                                        LatLngBounds.Builder::include
                                )
                                .map(LatLngBounds.Builder::build)
                                .toObservable(),
                        (options, zoomArea) -> new TracksViewState.Builder().tracks(options).zoomTo(zoomArea).build()
                )
        );
    }

}
