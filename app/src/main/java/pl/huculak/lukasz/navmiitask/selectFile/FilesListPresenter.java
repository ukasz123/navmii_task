package pl.huculak.lukasz.navmiitask.selectFile;

import com.hannesdorfmann.mosby3.mvi.MviPresenter;

/**
 * Presenter for GPX files
 * Created by Łukasz on 31.08.2017.
 */

public interface FilesListPresenter extends MviPresenter<FilesListView, FilesListViewState> {
}
