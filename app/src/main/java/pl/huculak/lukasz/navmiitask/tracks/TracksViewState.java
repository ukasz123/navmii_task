package pl.huculak.lukasz.navmiitask.tracks;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Represents state of the TracksView
 * Created by Łukasz on 30.08.2017.
 */

public class TracksViewState {

    private PolylineOptions tracks;
    private LatLngBounds zoomArea;


    private TracksViewState(PolylineOptions tracks, LatLngBounds zoomArea) {
        this.tracks = tracks;
        this.zoomArea = zoomArea;
    }

    public PolylineOptions getTracks() {
        return tracks;
    }

    public Builder builder() {
        return new Builder(this);
    }

    public LatLngBounds getZoomArea() {
        return zoomArea;
    }

    public static final class Builder {
        private PolylineOptions tracks;
        private LatLngBounds zoomArea;

        public Builder() {
        }

        private Builder(TracksViewState state) {
            this.tracks = state.tracks;
            this.zoomArea = state.zoomArea;
        }

        public TracksViewState build() {
            return new TracksViewState(tracks, zoomArea);
        }

        public Builder tracks(@NonNull PolylineOptions tracks) {
            this.tracks = tracks;
            return this;
        }

        public Builder zoomTo(@NonNull LatLngBounds zoomArea) {
            this.zoomArea = zoomArea;
            return this;
        }
    }
}
