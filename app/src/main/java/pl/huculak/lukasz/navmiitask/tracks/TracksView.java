package pl.huculak.lukasz.navmiitask.tracks;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * View that can display tracks
 * Created by Łukasz on 30.08.2017.
 */

public interface TracksView extends MvpView {

    /**
     * Event when map is ready
     * @return
     */
    Observable<Boolean> mapReady();

    /**
     * Displays the current state of the view
     *
     * @param currentState
     */
    void render(@NonNull TracksViewState currentState);
}
