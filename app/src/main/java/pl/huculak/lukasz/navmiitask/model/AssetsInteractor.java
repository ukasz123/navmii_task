package pl.huculak.lukasz.navmiitask.model;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;

import io.reactivex.Observable;

/**
 * Interactor for retrieving assets information
 * Created by Łukasz on 30.08.2017.
 */

public class AssetsInteractor {

    private static final String TRACKS_FOLDER_PATH = "tracks";
    private final AssetManager assets;

    public AssetsInteractor(Context context) {
        this.assets = context.getAssets();
    }

    public Observable<String> getAvailableGPXFiles() {
        return Observable.<String[]>fromPublisher(subscriber -> {
            try {
                String[] filePaths = assets.list(TRACKS_FOLDER_PATH);
                subscriber.onNext(filePaths);
                subscriber.onComplete();
            } catch (IOException e) {
                subscriber.onError(e);
            }
        }).flatMap(
                Observable::fromArray
        );
    }

    public InputStream openFileFromAssets(String path) throws IOException {
        return assets.open(TRACKS_FOLDER_PATH + "/" + path);
    }
}
