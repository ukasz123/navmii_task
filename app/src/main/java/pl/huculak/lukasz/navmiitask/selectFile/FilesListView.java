package pl.huculak.lukasz.navmiitask.selectFile;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Observable;

/**
 * Can display and select files
 * Created by Łukasz on 31.08.2017.
 */

public interface FilesListView extends MvpView {

    Observable<String> selectFileIntent();

    void render(FilesListViewState state);
}
